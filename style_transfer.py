# %% [markdown]
# # Image Style Transfer Using Convolutional Neural Networks
#
# This notebook implements the algorithm found in [(Gatys
# 2016)](https://www.cv-foundation.org/openaccess/content_cvpr_2016/papers/Gatys_Image_Style_Transfer_CVPR_2016_paper.pdf).

# %%
import torch
import torch.nn as nn
import torch.optim as optim

import torchvision.transforms as transforms
import torchvision.models as models
import torchvision.utils as utils

from PIL import Image

imsize = 128
loader = transforms.Compose([transforms.Resize(imsize), transforms.ToTensor()])

# Load `content_img' as a torch tensor of size 3 * `imsize' * `imsize'
image = Image.open("./data/images/dancing.jpg")
content_img = loader(image)

# Load `style_img' as a torch tensor of size 3 * `imsize' * `imsize'
image = Image.open("./data/images/paper.jpg")
style_img = loader(image)

# %% [markdown]

# ## Feature extraction with VGG19
# The next cell is a CNN based on VGG19 which extracts convolutional
# features specified by `modules_indexes'. It is used to compute the
# features of the content and style image. It is also used to
# reconstruct the target image by backpropagation.

# %%
class VGG19Features(nn.Module):
    def __init__(self, modules_indexes):
        super(VGG19Features, self).__init__()

        # VGG19 pretrained model in evaluation mode
        self.vgg19 = models.vgg19(pretrained=True).eval()

        # Indexes of layers to remember
        self.modules_indexes = modules_indexes

    def forward(self, input):
        # Define a hardcoded `mean' and `std' of size 3 * 1 * 1
        mean = torch.tensor([0.485, 0.456, 0.406]).view(-1, 1, 1)
        std = torch.tensor([0.229, 0.224, 0.225]).view(-1, 1, 1)

        raise NotImplementedError("Define `input_norm'")

        # Install hooks on specified modules to save their features
        features = []
        handles = []
        for module_index in self.modules_indexes:

            def hook(module, input, output):
                # `output' is of size (`batchsize' = 1) * `n_filters'
                # * `imsize' * `imsize'
                features.append(output)

            handle = self.vgg19.features[module_index].register_forward_hook(hook)
            handles.append(handle)

        # Forward propagate `input_norm'. This will trigger the hooks
        # set up above and populate `features'
        self.vgg19(input_norm)

        # Remove hooks
        [handle.remove() for handle in handles]

        # The output of our custom VGG19Features neural network is a
        # list of features of `input'
        return features


# %% [markdown]

# The next cell defines the convolutional layers we will use to
# capture the style and the content. Look at the paper to see what are
# those.

# %%

# Indexes of interesting features to extract

raise NotImplementedError("Define `modules_indexes'")

vgg19 = VGG19Features(modules_indexes)
content_features = [f.detach() for f in vgg19.forward(content_img)]

# %% [markdown]

# ## Style features as gram matrix of convolutional features

# The next cell computes the gram matrix of `input'. We first need to
# reshape `input' before computing the gram matrix.

# %%
def gram_matrix(input):
    batchsize, n_filters, width, height = input.size()

    # Reshape `input' into `n_filters' * `n_pixels'
    raise NotImplementedError("Define `features'")

    # Compute the inner products between filters
    raise NotImplementedError("Define `G'")

    # We 'normalize' the values of the gram matrix by dividing by the
    # number of element in each feature maps.
    return G.div(n_filters * width * height)


style_gram_features = [gram_matrix(f.detach()) for f in vgg19.forward(style_img)]

target = content_img.clone().requires_grad_(True)

# %% [markdown]

# ## Optimizer

# Look at the paper to see what is the algorithm they are using.
# Remember that we are optimizing on a target image.

# %%

# Define `optimizer' to use L-BFGS algorithm to do gradient descent on
# `target'
optimizer = __

# %% [markdown]

# ## The algorithm

# From the paper, there are two different losses. The style loss and the
# content loss.

# Define `style_weight' the trade-off parameter between style and
# content losses
raise NotImplementedError("Define `style_weight'")

# %%

for step in range(500):
    # First, forward propagate `target' through our VGG19Features neural
    # network and store its output as `target_features'
    raise NotImplementedError("Define `target_features'")

    # Define `content_loss' on the first layer only
    raise NotImplementedError("Define `content_loss'")

    style_loss = 0
    for target_feature, style_gram_feature in zip(target_features, style_gram_features):
        raise NotImplementedError("Compute gram matrix")

    # Compute combined loss
    raise NotImplementedError

    # Backpropagate gradient and optimize
    optimizer.zero_grad()
    loss.backward()

    optimizer.step(lambda: loss)

    if step % 20 == 0:
        print("step {}:".format(step))
        print(
            "Style Loss: {:4f} Content Loss: {:4f} Overall: {:4f}".format(
                style_loss.item(), content_loss.item(), loss.item()
            )
        )
        img = target.clone().squeeze()
        img = img.clamp_(0, 1)
        utils.save_image(img, "output-{}.png".format(step))
